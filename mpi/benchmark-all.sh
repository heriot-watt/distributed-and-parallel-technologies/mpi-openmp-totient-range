#!/bin/bash
#Anthony Genson 18/03/2020

#SBATCH --partition=amd-longq
#SBATCH -N4 -n 256
#SBATCH -J totient
#SBATCH --error=sbatch-benchmark-all.err
#SBATCH --output=sbatch-benchmark-all.out

if [[ "$#" -ne 1 ]]; then
    echo "Error: needs 1 argument (directory of codes)." 1>&2
    exit 128
elif [[ "$1" != "opti" && "$1" != "opti/" && "$1" != "opti-gcd" && "$1" != "opti-gcd/" ]]; then
    echo "Error: wrong argument ('opti' or 'opti-gcd')." 1>&2
    exit 128
fi

cd $1

# Clear all loaded modules
module prune > /dev/null 2>&1

# Load necessary modules for the job
module load gcc slurm intel/mpi/64

OUTPUT_PREFIX="../benchmark"

[[ ! -d "$OUTPUT_PREFIX" ]] && mkdir $OUTPUT_PREFIX

declare -a array=(15000 30000 100000)

for try in {1..3}; do
    for ds in ${array[@]}; do
	for code in *.c; do
	    filename="${code%.c}"
	    output="${OUTPUT_PREFIX}/${filename}-${try}-${ds}.csv"

	    [[ -f "$output" ]] && rm $output

	    echo "Code,Tasks,Chunksize,Lower,Upper,Sum,Time" >> $output

	    # Compile
	    mpicc -O3 $code -o $SLURM_JOB_NAME.out

	    if [[ "$filename" == "totient-seq" || "$filename" == "totient-gcd-seq" ]]; then
		# Run seq separately
		srun -n1 --mpi=pmi2 $SLURM_JOB_NAME.out 1 $ds 8 >> $output
	    else
		for ((cores=1; cores<=256; cores*=2)); do
		    # Run the job
		    srun -n$cores --mpi=pmi2 $SLURM_JOB_NAME.out 1 $ds 0 >> $output
		done
	    fi

	    # Clean
	    rm $SLURM_JOB_NAME.out
	done
    done
done
