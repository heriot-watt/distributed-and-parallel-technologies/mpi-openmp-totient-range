#!/bin/bash
#Anthony Genson 18/03/2020

#SBATCH --partition=amd-longq
#SBATCH -N4 -n 256
#SBATCH -J totient
#SBATCH --error=sbatch-chunksize.err
#SBATCH --output=sbatch-chunksize.out

if [[ "$#" -ne 1 ]]; then
    echo "Error: needs 1 argument (directory of codes)." 1>&2
    exit 128
elif [[ "$1" != "opti" && "$1" != "opti/" && "$1" != "opti-gcd" && "$1" != "opti-gcd/" ]]; then
    echo "Error: wrong argument ('opti' or 'opti-gcd')." 1>&2
    exit 128
fi

cd $1

# Clear all loaded modules
module prune > /dev/null 2>&1

# Load necessary modules for the job
module load gcc slurm intel/mpi/64

OUTPUT_PREFIX="../benchmark"

[[ ! -d "$OUTPUT_PREFIX" ]] && mkdir $OUTPUT_PREFIX

declare -a types=('dyn' 'gui')
declare -a codes=('.' '-opti.' '-opti-avail.' '-opti-notify.' '-opti-notify-avail.')

for ctype in ${types[@]}; do
    if [[ "$1" == "opti-gcd" || "$1" == "opti-gcd/" ]]; then
	OUTPUT="${OUTPUT_PREFIX}/chunksize-gcd-${ctype}.csv"
    else
	OUTPUT="${OUTPUT_PREFIX}/chunksize-${ctype}.csv"
    fi

    [[ -f "$OUTPUT" ]] && rm $OUTPUT

    echo "Code,Tasks,Chunksize,Lower,Upper,Sum,Time" >> $OUTPUT

    for ((cores=1; cores<=256; cores*=2)); do
	for code in ${codes[@]}; do
	    if [[ "$1" == "opti-gcd" || "$1" == "opti-gcd/" ]]; then
		# Compile
		mpicc -O3 totient-gcd-${ctype}${code}c -o $SLURM_JOB_NAME.out
	    else
		# Compile
		mpicc -O3 totient-${ctype}${code}c -o $SLURM_JOB_NAME.out
	    fi

	    # Run the job
	    srun -n$cores --mpi=pmi2 $SLURM_JOB_NAME.out 0 15000 >> $OUTPUT

	    # Clean
	    rm $SLURM_JOB_NAME.out
	done
    done
done
