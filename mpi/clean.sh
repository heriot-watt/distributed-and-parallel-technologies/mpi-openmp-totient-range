#Anthony Genson 18/03/2020

find . -type f -name '*~' -delete
find . -type f -name '*#' -delete
find . -type f -name '*.out' -delete
find . -type f -name '*.err' -delete

for elt in "$@"; do
    find . -type f -name $elt -delete
done
