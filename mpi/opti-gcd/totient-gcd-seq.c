// Greg Michaelson 14/10/2003
// Patrick Maier   29/01/2010 [enforced ANSI C compliance]
// Anthony Genson  18/03/2020

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

// This function is a work of Prof. Daniel Lemire
// https://lemire.me/blog/2013/12/26/fastest-way-to-compute-the-greatest-common-divisor/
unsigned long gcd(unsigned long u, unsigned long v) {
  int shift, uz, vz;
  long diff;

  if (u == 0) return v;
  if (v == 0) return u;

  uz = __builtin_ctzl(u);
  vz = __builtin_ctzl(v);
  shift = uz > vz ? vz : uz;
  u >>= uz;

  do {
    v >>= vz;
    diff = v - u;
    vz = __builtin_ctzl(diff);

    if (diff == 0) break;
    if (v < u) u = v;

    v = labs(diff);
  } while(1);

  return u << shift;
}

long hcf(long x, long y) {
  long t;

  while (y != 0) {
    t = x % y;
    x = y;
    y = t;
  }

  return x;
}

int relprime(long x, long y) {
  return (x < 0 || y < 0 ? hcf(x, y) : gcd(x, y)) == 1;
}

long euler(long n) {
  long length = 0;

  for (int i = 1; i < n; i++)
    if (relprime(n, i))
      length++;

  return length;
}

long sumTotient(long lower, long upper) {
  long sum = 0;

  for (int i = lower; i <= upper; i++)
    sum = sum + euler(i);

  return sum;
}

void runBenchmark(long lower, long upper) {
  clock_t start, end;
  double time_taken;
  long sum;

  start = clock();
  sum = sumTotient(lower, upper);
  end = clock();

  time_taken = ((double) (end - start)) / CLOCKS_PER_SEC;
  printf("gcd-seq,1,0,%ld,%ld,%ld,%.2f\n", lower, upper, sum, time_taken);
}

int main(int argc, char ** argv) {
  long lower, upper;

  if (argc < 3) {
    fprintf(stderr, "Error: need 2 arguments.\n");
    return 1;
  }

  sscanf(argv[1], "%ld", &lower);
  sscanf(argv[2], "%ld", &upper);
  runBenchmark(lower, upper);

  return 0;
}
