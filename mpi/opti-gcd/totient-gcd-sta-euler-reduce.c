// Greg Michaelson 14/10/2003
// Patrick Maier   29/01/2010 [enforced ANSI C compliance]
// Anthony Genson  18/03/2020

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <mpi.h>

// This function is a work of Prof. Daniel Lemire
// https://lemire.me/blog/2013/12/26/fastest-way-to-compute-the-greatest-common-divisor/
unsigned long gcd(unsigned long u, unsigned long v) {
  int shift, uz, vz;
  long diff;

  if (u == 0) return v;
  if (v == 0) return u;

  uz = __builtin_ctzl(u);
  vz = __builtin_ctzl(v);
  shift = uz > vz ? vz : uz;
  u >>= uz;

  do {
    v >>= vz;
    diff = v - u;
    vz = __builtin_ctzl(diff);

    if (diff == 0) break;
    if (v < u) u = v;

    v = labs(diff);
  } while(1);

  return u << shift;
}

long hcf(long x, long y) {
  long t;

  while (y != 0) {
    t = x % y;
    x = y;
    y = t;
  }

  return x;
}

int relprime(long x, long y) {
  return (x < 0 || y < 0 ? hcf(x, y) : gcd(x, y)) == 1;
}

long euler(long n, int comm_size, int comm_rank) {
  long glength = 0, llength = 0;

  // Divide work between processes
  for (int i = comm_rank + 1; i < n; i += comm_size)
    if (relprime(n, i))
      llength++;

  // Each process will have the result
  MPI_Reduce(&llength, &glength, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

  return glength;
}

long sumTotient(long lower, long upper, int comm_size, int comm_rank) {
  long sum = 0;

  // All processes repeat this loop
  for (int i = lower; i <= upper; i++)
    sum = sum + euler(i, comm_size, comm_rank);

  return sum;
}

void runBenchmark(long lower, long upper, int comm_size, int comm_rank) {
  double start, end, time_taken;
  long sum;

  start = MPI_Wtime();
  sum = sumTotient(lower, upper, comm_size, comm_rank);
  MPI_Barrier(MPI_COMM_WORLD);
  end = MPI_Wtime();

  if (comm_rank == 0) {
    time_taken = end - start;
    printf("gcd-sta-euler-reduce,%d,0,%ld,%ld,%ld,%.2f\n", comm_size, lower, upper, sum, time_taken);
  }
}

int main(int argc, char **argv) {
  int comm_size, comm_rank;
  long lower, upper;

  if (argc < 3) {
    fprintf(stderr, "Error: need 2 arguments.\n");
    return 1;
  }

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &comm_rank);

  sscanf(argv[1], "%ld", &lower);
  sscanf(argv[2], "%ld", &upper);
  runBenchmark(lower, upper, comm_size, comm_rank);

  return 0;
}
