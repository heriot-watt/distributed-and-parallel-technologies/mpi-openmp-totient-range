// Greg Michaelson 14/10/2003
// Patrick Maier   29/01/2010 [enforced ANSI C compliance]
// Anthony Genson  18/03/2020

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <mpi.h>

#define CHUNKSIZE 8

long hcf(long x, long y) {
  long t;

  while (y != 0) {
    t = x % y;
    x = y;
    y = t;
  }

  return x;
}

int relprime(long x, long y) {
  return hcf(x, y) == 1;
}

long euler(long n) {
  long length = 0;

  for (int i = 1; i < n; i++)
    if (relprime(n, i))
      length++;

  return length;
}

long sumTotientSeq(long lower, long upper) {
  long sum = 0;

  for (long i = lower; i <= upper; i++)
    sum += euler(i);

  return sum;
}

MPI_Request* allocRequests(int n) {
  MPI_Request *requests = (MPI_Request*) malloc(n * sizeof(MPI_Request));

  for (int i = 0; i < n; i++)
    requests[i] = MPI_REQUEST_NULL;

  return requests;
}

long getChunksize(long index, long upper, int nb_workers, long min_chunksize) {
  long chunksize, iterations = upper+1 - index;

  chunksize = iterations / nb_workers;

  if (chunksize < min_chunksize)
    chunksize = min_chunksize;

  if (iterations < chunksize)
    chunksize = iterations;

  return chunksize;
}

long sumTotientParMaster(long lower, long upper, int nb_workers, long min_chunksize) {
  long *data = calloc(getChunksize(lower, upper, nb_workers, min_chunksize), sizeof(long));
  long chunksize, partial_sum, sum = 0, index = lower;
  MPI_Request *requests = allocRequests(nb_workers);
  int flag, req_count = 0, rcv_count = 0;
  MPI_Request req;

  do {
    // For each request
    for (int i = 0; i < nb_workers; i++) {
      if (requests[i] != MPI_REQUEST_NULL) {
	// Test if request is still pending
	MPI_Test(&(requests[i]), &flag, MPI_STATUS_IGNORE);

	// If finished, count received
        if (flag)
	  rcv_count++;
      }
    }

    // For each worker, as long as index <= upper
    for (int j = 0; (j < nb_workers) && (index <= upper); j++) {
      // If available
      if (requests[j] == MPI_REQUEST_NULL) {
	// Recalculate chunksize
	chunksize = getChunksize(index, upper, nb_workers, min_chunksize);

	// Prepare chunk of data
        for (long k = 0; k < chunksize; k++)
          data[k] = index++;

	// Send chunksize + chunk of data
	MPI_Isend(&chunksize, 1, MPI_LONG, j+1, 0, MPI_COMM_WORLD, &req);
        MPI_Isend(data, chunksize, MPI_LONG, j+1, 0, MPI_COMM_WORLD, &req);
	// Non-blocking receiver, stored as pending request
        MPI_Irecv(NULL, 0, MPI_C_BOOL, j+1, 0, MPI_COMM_WORLD, &(requests[j]));
	// Count requested
	req_count++;
      }
    }
    // Continue looping as long as workers are processing data
  } while ((index <= upper) || (req_count != rcv_count));

  // Inform each worker of exit and sum all partial results
  for (int l = 0; l < nb_workers; l++) {
    MPI_Send(NULL, 0, MPI_C_BOOL, l+1, 1, MPI_COMM_WORLD);
    MPI_Recv(&partial_sum, 1, MPI_LONG, l+1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    sum += partial_sum;
  }

  free(data);
  free(requests);
  return sum;
}

void sumTotientParWorker() {
  MPI_Request stop_request, recv_request = MPI_REQUEST_NULL;
  long chunksize, local_sum = 0;
  long *data = NULL;
  int flag;

  // Create pending request for exit status
  MPI_Irecv(NULL, 0, MPI_C_BOOL, 0, 1, MPI_COMM_WORLD, &stop_request);

  while (true) {
    // Test exit status
    MPI_Test(&stop_request, &flag, MPI_STATUS_IGNORE);

    if (flag) {
      if (recv_request != MPI_REQUEST_NULL)
        MPI_Cancel(&recv_request);

      break;
    }

    // Request or receive chunksize
    if (recv_request == MPI_REQUEST_NULL)
      MPI_Irecv(&chunksize, 1, MPI_LONG, 0, 0, MPI_COMM_WORLD, &recv_request);
    else
      MPI_Test(&recv_request, &flag, MPI_STATUS_IGNORE);

    // If data received
    if (flag) {
      // Allocate memory for data if not already done (first chunksize = biggest)
      if (data == NULL)
	data = calloc(chunksize, sizeof(long));

      // Receive chunk of data from master
      MPI_Recv(data, chunksize, MPI_LONG, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

      // Calculate euleur & sum for each element
      for (long i = 0; i < chunksize; i++)
	local_sum += euler(data[i]);

      // Notify master of availability
      MPI_Send(NULL, 0, MPI_C_BOOL, 0, 0, MPI_COMM_WORLD);
    }
  }

  // Send partial sum to master
  MPI_Send(&local_sum, 1, MPI_LONG, 0, 0, MPI_COMM_WORLD);

  if (data != NULL)
    free(data);
}

long sumTotientPar(long lower, long upper, int comm_size, int comm_rank, long min_chunksize) {
  long sum = -1;

  // Only the root process will have the result, the others return -1 
  if (comm_rank == 0)
    sum = sumTotientParMaster(lower, upper, comm_size - 1, min_chunksize);
  else
    sumTotientParWorker();

  return sum;
}

void benchmark(long lower, long upper, int comm_size, int comm_rank, long min_chunksize) {
  double start, end, time_taken;
  long sum = -1;

  start = MPI_Wtime();

  // Needs more than 1 process for master/worker(s) architecture
  // Otherwise runs in sequential
  if (comm_size < 2) {
    if (comm_rank == 0)
      sum = sumTotientSeq(lower, upper);
  } else
    sum = sumTotientPar(lower, upper, comm_size, comm_rank, min_chunksize);

  MPI_Barrier(MPI_COMM_WORLD);
  end = MPI_Wtime();
  
  if (comm_rank == 0) {
    time_taken = end - start;
    printf("gui-opti-notify,%d,%ld,%ld,%ld,%ld,%.2f\n", comm_size, min_chunksize, lower, upper, sum, time_taken);
  }
}

void loopBenchmark(long lower, long upper, int comm_size, int comm_rank) {
  long chunksize = 1;

  // Runs benchmark on different chunksize (x2 for each iteration)
  while (true) {
    benchmark(lower, upper, comm_size, comm_rank, chunksize);

    if (chunksize == upper)
      break;
    else
      chunksize = chunksize*2 >= upper ? upper : chunksize*2;
  }
}

void runBenchmark(long lower, long upper, int comm_size, int comm_rank, long chunksize) {
  // Runs with default chunksize
  if (chunksize == 0)
    benchmark(lower, upper, comm_size, comm_rank, CHUNKSIZE);
  // Runs with given chunksize
  else if (chunksize > 0)
    benchmark(lower, upper, comm_size, comm_rank, chunksize);
  // Runs loop of different chunksize
  else
    loopBenchmark(lower, upper, comm_size, comm_rank);
}

int main(int argc, char ** argv) {
  long lower, upper, chunksize = -1;
  int comm_size, comm_rank;

  if (argc != 3 && argc != 4) {
    if (comm_rank == 0)
      fprintf(stderr, "Error: need 2-3 arguments (lower upper [chunksize]).\n");
    
    return 1;
  }

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &comm_rank);

  sscanf(argv[1], "%ld", &lower);
  sscanf(argv[2], "%ld", &upper);

  if (argc == 4)
    sscanf(argv[3], "%ld", &chunksize);

  runBenchmark(lower, upper, comm_size, comm_rank, chunksize);

  MPI_Finalize();
  return 0;
}
