// Greg Michaelson 14/10/2003
// Patrick Maier   29/01/2010 [enforced ANSI C compliance]
// Anthony Genson  18/03/2020

#include <stdio.h>
#include <time.h>
#include <mpi.h>

long hcf(long x, long y) {
  long t;

  while (y != 0) {
    t = x % y;
    x = y;
    y = t;
  }

  return x;
}

int relprime(long x, long y) {
  return hcf(x, y) == 1;
}

long euler(long n) {
  long length = 0;

  for (int i = 1; i < n; i++)
    if (relprime(n, i))
      length++;

  return length;
}

long sumTotient(long lower, long upper, int comm_size, int comm_rank) {
  long lsum = 0, gsum = 0;

  // Divide work between processes
  for (int i = lower + comm_rank; i <= upper; i += comm_size)
    lsum = lsum + euler(i);

  // Each process will have the result
  MPI_Reduce(&lsum, &gsum, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

  return gsum;
}

void runBenchmark(long lower, long upper, int comm_size, int comm_rank) {
  double start, end, time_taken;
  long sum;

  start = MPI_Wtime();
  sum = sumTotient(lower, upper, comm_size, comm_rank);
  MPI_Barrier(MPI_COMM_WORLD);
  end = MPI_Wtime();
  
  if (comm_rank == 0) {
    time_taken = end - start;
    printf("sta-reduce,%d,0,%ld,%ld,%ld,%.2f\n", comm_size, lower, upper, sum, time_taken);
  }
}

int main(int argc, char **argv) {
  int comm_size, comm_rank;
  long lower, upper;

  if (argc < 3) {
    fprintf(stderr, "Error: need 2 arguments.\n");
    return 1;
  }

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &comm_rank);

  sscanf(argv[1], "%ld", &lower);
  sscanf(argv[2], "%ld", &upper);
  runBenchmark(lower, upper, comm_size, comm_rank);

  MPI_Finalize();
  return 0;
}
