#!/bin/bash
#Anthony Genson 18/03/2020

#SBATCH --partition=amd-longq
#SBATCH -t00:05:00
#SBATCH -N1 -n4
#SBATCH -J totient
#SBATCH --error=sbatch-test-all.err
#SBATCH --output=sbatch-test-all.out

if [[ "$#" -ne 1 ]]; then
    echo "Error: needs 1 argument (directory of codes)." 1>&2
    exit 128
elif [[ "$1" != "opti" && "$1" != "opti/" && "$1" != "opti-gcd" && "$1" != "opti-gcd/" ]]; then
    echo "Error: wrong argument ('opti' or 'opti-gcd')." 1>&2
    exit 128
fi

cd $1

# Clear all loaded modules
module prune > /dev/null 2>&1

# Load necessary modules for the job
module load gcc slurm intel/mpi/64

echo "Code,Tasks,Chunksize,Lower,Upper,Sum,Time"

for code in *.c; do
    # Compile
    mpicc -O3 $code -o $SLURM_JOB_NAME.out

    # Run the job
    srun --mpi=pmi2 $SLURM_JOB_NAME.out 0 10000 0

    # Clean
    rm $SLURM_JOB_NAME.out
done
