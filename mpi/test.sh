#!/bin/bash
#Anthony Genson 18/03/2020

#SBATCH --partition=amd-longq
#SBATCH -t00:05:00
#SBATCH -N1 -n4
#SBATCH -J totient
#SBATCH --error=sbatch-test.err
#SBATCH --output=sbatch-test.out

if [[ "$#" -ne 1 ]]; then
    echo "Error: needs 1 argument (code)." 1>&2
    exit 128
fi

# Clear all loaded modules
module prune > /dev/null 2>&1

# Load necessary modules for the job
module load gcc slurm intel/mpi/64

echo "Code,Tasks,Chunksize,Lower,Upper,Sum,Time"

# Compile
mpicc -O3 $1 -o $SLURM_JOB_NAME.out

# Run the job
srun --mpi=pmi2 $SLURM_JOB_NAME.out 0 10000 0

# Clean
rm $SLURM_JOB_NAME.out
