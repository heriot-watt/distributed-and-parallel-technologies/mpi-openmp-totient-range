// TotientRance.c - Sequential Euler Totient Function (C Version)
// compile: gcc -Wall -O -o TotientRange TotientRange.c
// run:     ./TotientRange lower_num uppper_num

// Greg Michaelson 14/10/2003
// Patrick Maier   29/01/2010 [enforced ANSI C compliance]
// Hugo Cousin     18/03/2020 [adding optmised half gcd]

// This program calculates the sum of the totients between a lower and an 
// upper limit using C longs. It is based on earlier work by:
// Phil Trinder, Nathan Charles, Hans-Wolfgang Loidl and Colin Runciman

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

// This function is a work of Prof. Daniel Lemire
// https://lemire.me/blog/2013/12/26/fastest-way-to-compute-the-greatest-common-divisor/
unsigned long gcd(unsigned long u, unsigned long v)
{
  int shift, uz, vz;
  if ( u == 0) return v;
  if ( v == 0) return u;
  uz = __builtin_ctzl(u);
  vz = __builtin_ctzl(v);
  shift = uz > vz ? vz : uz;
  u >>= uz;
  do {
    v >>= vz;
    long diff = v;
    diff -= u;
    vz = __builtin_ctzl(diff);
    if ( diff == 0 ) break;
    if ( v <  u )
      u = v;
    v = labs(diff);

  } while( 1 );
  return u << shift;
}

long hcf(long x, long y)
{
  long t;

  while (y != 0) {
    t = x % y;
    x = y;
    y = t;
  }
  return x;
}

int relprime(long x, long y)
{
  return (x < 0 || y < 0 ? hcf(x, y) : gcd(x, y)) == 1;
}

long euler(long n)
{
  long length, i;

  length = 0;
  for (i = 1; i < n; i++)
    if (relprime(n, i))
      length++;
  return length;
}

long sumTotient(long lower, long upper)
{
  long sum, i;

  sum = 0;
  for (i = lower; i <= upper; i++)
    sum = sum + euler(i);
  return sum;
}


void runBenchmark(long lower, long upper)
{
  clock_t start, end;
  double time_taken;

  start = clock();
  long sum = sumTotient(lower, upper);
  end = clock();
  time_taken = ((double) (end - start)) / CLOCKS_PER_SEC;
  printf("%f\n", time_taken);
}

int main(int argc, char ** argv)
{
  long lower, upper;

  if (argc != 3) {
    printf("not 2 arguments\n");
    return 1;
  }
  sscanf(argv[1], "%ld", &lower);
  sscanf(argv[2], "%ld", &upper);
  runBenchmark(lower, upper);
  return 0;
}

